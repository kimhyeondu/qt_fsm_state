#ifndef IMAGESQLTABLEMODEL_H
#define IMAGESQLTABLEMODEL_H

#include <qobject.h>
#include<QSqlTableModel>

class ImageSqlTableModel : public QSqlTableModel
{
public:
    ImageSqlTableModel( QObject* parent = nullptr, QSqlDatabase db = QSqlDatabase() );

    // QAbstractItemModel interface
    virtual QVariant data( const QModelIndex& index, int role ) const override;
};

#endif // IMAGESQLTABLEMODEL_H
