#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "database.h"
#include <QMainWindow>
#include <QPushButton>
#include <QTextEdit>
#include <QtSql/QSqlDatabase>
#include "imagesqltablemodel.h"
#include "pcbstatemachine.h"





QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
 QSqlTableModel* model         = new ImageSqlTableModel( this, m_pManagerDataBase->init_database() );
    void on_insert_button_clicked();
    void on_pushButton_clicked();
    void on_delet_button_clicked();
    void on_show_button_clicked();
    void on_img_button_clicked();
           void on_next_state_clicked();
           void on_temp_data_print(QPixmap);
            void on_hum_data_print(QPixmap);
             void on_rain_data_print(QPixmap);
              void on_img_data_print(QPixmap);
 //  database *database_class = database::getInstance();

signals:
    void signals_insert_db();
private slots:



private:
   QStandardItemModel *Item_model = new QStandardItemModel;
    database* m_pManagerDataBase = database::getInstance();

    QSqlQueryModel* qryModel      = new ImageSqlTableModel( this, m_pManagerDataBase->init_database() );
    QTableModel *table_model;
   PcbStateMachine*        m_pStateMachine  = new PcbStateMachine( this );


    Ui::MainWindow *ui;

    //singleton inspection

};
#endif // MAINWINDOW_H
