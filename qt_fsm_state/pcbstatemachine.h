#ifndef PCBSTATEMACHINE_H
#define PCBSTATEMACHINE_H
#include "stable.h"
#include <mainconfig.h>
#include "data_select.h"

Q_DECLARE_LOGGING_CATEGORY( sm )

class PcbStateMachine : public QObject
{
    Q_OBJECT
public:
    explicit PcbStateMachine( QObject* parent = nullptr );
     data_select databox;


signals:
    // state machine
    void signalInitState();
    void signalNextState();
    void signalNext1State();
    void signalNext2State();
    void signalRetryState();
    void signalRecheckState();
    void signalPrepareState();
    //
   //void signalStateButtonClick(QTableView);
  void signalTempMessage(QPixmap);
    void signalHumMessage(QPixmap);
      void signalRainMessage(QPixmap);
        void signalImageMessage(QPixmap);
public slots:
private slots:
    // onState
    void onS00StartEntered();
    void onS10PrepareEntered();
    void onS20LocationEntered();
    void onS30DetectModeEntered();
    void onS31ManualDetectEntered();
//    void onS32AutoDetectEntered();
//    void onS50BarcodeEntered();
//    void onS70InspectionEntered();
//    void onS90ResultEntered();
private:
    enum LocationType { GENERAL, COMPACT, COMPACT_2, GENERAL_2 };
    //

    bool                                m_bTestMode         = false;
    int                                 m_nLocationBounce   = 0;
    cv::Mat                             m_mSourceImage; // original image
    cv::RotatedRect                     m_LastRotatedRect;
    // singleton instance
    bool                                m_bNewInspection    = true;
    QSqlDatabase                        m_MssqlDb;
    // state machine
    QStateMachine*                      m_pStateMachine     = new QStateMachine;
    QState*                             m_pStateRoot        = new QState( m_pStateMachine );
    QState*                             m_pState00          = new QState( m_pStateRoot );
    QState*                             m_pState10          = new QState( m_pStateRoot );
    QState*                             m_pState20          = new QState( m_pStateRoot );
    QState*                             m_pState30          = new QState( m_pStateRoot );
     QState*                             m_pState31          = new QState( m_pStateRoot );

    QSignalTransition*                  m_pTransRetry00;
    //
    QList<cv::Mat>                      m_CurrentImageList;

    void initializeStateMachine();

};

#endif // PCBSTATEMACHINE_H
