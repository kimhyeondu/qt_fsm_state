#ifndef MAINCONFIG_H
#define MAINCONFIG_H
#include "stable.h"

class MainConfig : public QObject
{
    Q_OBJECT

private:
    MainConfig( const QString& fileName = "./model/config.ini", QSettings::Format format = QSettings::IniFormat, QObject* parent = nullptr );
    ~MainConfig();

    QSettings m_rMainSetting;

public:
    static MainConfig& getInstance();
    QList<QString> cameraSerailList;

    enum DETECT_MODE { AUTO_VISION, MANUAL_SENSOR };
    Q_ENUM( DETECT_MODE )


    // [DefaultValue]--------------------------------------------------
    int defaultBarcodeCheck         = 1;
    int defaultImageSave            = 1;
    int defaultSoundFlag            = 2; // 0:off, 1:pass_only, 2:fail_only, 3:on
    int defaultImageRotate          = 0; // 0:0, 1:90, 2:180, 3:270
    int defaultDetectionLine        = 48;
    int defaultLocationType         = 0; // 0:general type, 1:compact(for harness) type, 2:compact2(array for inline) type
    int defaultIsArrayForCompact    = 0; // 0:not array, 1:array (only apply compact type)
    int defaultIsSimpleForCompact   = 0; // 0:not panel, 1:panel (only apply compact type)
    int defaultCompact2MaxLimit     = 75;
    int defaultEmptyCheck           = 1;
    int defaultEmptyLimit           = 70;
    int defaultReverseCheck         = 0;  // connector
    int defaultReverseLimit         = 70; // connector
    int defaultCapacitorCheck       = 0;
    int defaultCapacitorLimit       = 70;// Capacitor
    int defaultCapacitorMin         = 100;
    int defaultCapacitorMax         = 200;
    int defaultColorCheck           = 0;
    int defaultColorLimit           = 75;
    int defaultSearchRangeView      = 0;
    int defaultSearchRangeUpDown    = 25;
    int defaultSearchRangeLeftRight = 25;
    int defaultRACThreshold         = 50;
    int defaultRACMopologyRepeat    = 21;
    int defaultBarcodeRow           = 1;
    int defaultBarcodeColumn        = 1;
    int defaultBarcodeSize          = 60;
    // [Setting]--------------------------------------------------
    // operation
    int     TempImageSave               = 0;
    int     DetectionMode               = 0;// 0: auto(vision), 1:manual(sensor)
    int     ManualDetectDelay           = 0;
    int     RacBlackThreshold           = 12;
    int     MinimumDetectSize           = 400; // px
    int     MainImageRoate              = 0;   // 0:0, 1:90, 2:180, 3:270
    int     FlowDirection               = 0;   // 0:up->down, 1:left->right, 2:down->up, 3:right->left
    int     CompactModeMin              = 400; // hessian?
    int     Compact2Degree              = 3;   // both side(left, right) => result 7(3+3+1)
    int     ExtendLocation              = 0;   // append location process
    int     AutoManualDetectInSetting   = 3;   // 0: auto(x), manual(x), 1: auto(x), manual(o), 2: auto(o), manual(x), 3: auto(o), manual(o)
    QString CommPort                    = "";
    QString EquipId                     = "";
    QString LastModelName               = "";
    // result image save
    QString ResultSavePath              = "./save";
    int     KeepMonths                  = 3;// months
    // logs
    QString LogSavePath                 = "./logs";
    QString InitLogLevel                = "info";
    // password for setting
    QString SettingPassword             = "";
    // fail color
    int     NgColorEmptyRed             = 255;
    int     NgColorEmptyGreen           = 0;
    int     NgColorEmptyBlue            = 0;
    int     NgColorReverseRed           = 255;
    int     NgColorReverseGreen         = 0;
    int     NgColorReverseBlue          = 0;
    int     NgColorColorRed             = 255;
    int     NgColorColorGreen           = 0;
    int     NgColorColorBlue            = 0;
    // sound
    QString OkSoundPath                 = "sound/ok.wav";
    QString NgSoundPath                 = "sound/ng.wav";
    QString BarcodeSoundPath            = "sound/barcode.wav";
    int     BarcodeErrorSound           = 0;
    // background noise remove
    int     BgNoiseRemove               = 0;
    int     BgNoiseMinS                 = 4;
    int     BgNoiseMaxS                 = 112;
    int     BgNoiseMinV                 = 70;
    int     BgNoiseMaxV                 = 140;
    // watching
    QString WatchingSavePath            = "";
    QString WatchingDbIp                = "";
    QString WatchingDbName              = "";
    QString WatchingDbId                = "";
    QString WatchingDbPw                = "";
    // enable view
    bool    enabledReverseAll           = true; // connector
    bool    enabledReverseUpDown        = true; // connector
    bool    enabledReverseLeftRight     = true; // connector
    bool    enabledReverseDetail        = true; // connector
    // check
    QString check                       = "";
    //
    void readAll();
    void writeAll();
    void writeLastModelName     ( const QString& modelname );
    bool writeSettingPassword   ( const QString& password );
    bool checkPassword          ( const QString& password );
};

#endif // MAINCONFIG_H
