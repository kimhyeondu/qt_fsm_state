QT       += core gui sql concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    data_select.cpp \
    database.cpp \
    imagesqltablemodel.cpp \
    main.cpp \
    mainwindow.cpp \
    pcbstatemachine.cpp

HEADERS += \
    data_select.h \
    database.h \
    imagesqltablemodel.h \
    mainconfig.h \
    mainwindow.h \
    pcbstatemachine.h \
    stable.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
#-------------------------------------------------
# OPENCV 4.1.0
#-------------------------------------------------
BUILD_INSTALL = gneetech_lib #opencv/opencv_install_4.1.0

win32:CONFIG(release, debug|release){
    LIBS += -L$$PWD/../../../$$BUILD_INSTALL/lib/opencv -lopencv_world410 -lopencv_img_hash410 -lippiw -lippicvmt
    LIBS += -lzlib -llibjpeg-turbo -llibpng -llibtiff -llibjasper -llibwebp -littnotify -lIlmImf -lquirc -lade -llibprotobuf
}
else:win32:CONFIG(debug, debug|release){
    LIBS += -L$$PWD/../../../$$BUILD_INSTALL/lib/opencv -lopencv_world410d -lopencv_img_hash410d -lippiwd -lippicvmt
    LIBS += -lzlibd -llibjpeg-turbod -llibpngd -llibtiffd -llibjasperd -llibwebpd -littnotifyd -lIlmImfd -lquircd -lade -llibprotobufd
}

# opencv, tesseract, leptonica...
INCLUDEPATH += $$PWD/../../../$$BUILD_INSTALL/include/opencv
DEPENDPATH  += $$PWD/../../../$$BUILD_INSTALL/include/opencv


##-------------------------------------------------
## SimVision2
##-------------------------------------------------
CONFIG(release, debug|release): LIBS += -L$$PWD/../../../gneetech_lib/lib/simvision -lSimVision
CONFIG(debug,   debug|release): LIBS += -L$$PWD/../../../gneetech_lib/lib/simvision -lSimVisiond

INCLUDEPATH += $$PWD/../../../gneetech_lib/include/simvision
DEPENDPATH  += $$PWD/../../../gneetech_lib/include/simvision

DISTFILES += \
    images/1.png
