#ifndef DATA_SELECT_H
#define DATA_SELECT_H
#include "database.h"
#include "imagesqltablemodel.h"

class data_select
{
public:
    ImageSqlTableModel *state_table = new ImageSqlTableModel(nullptr,database::getInstance()->init_database());
   QTableView *view = new QTableView;
    data_select();
    void temp_data(QTableView *table);
    void hum_data(QTableView *table);
    void rain_data(QTableView *table);
    void image_data(QTableView* table);
    ~data_select();
};

#endif // DATA_SELECT_H
