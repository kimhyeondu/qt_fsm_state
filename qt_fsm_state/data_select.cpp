#include "data_select.h"

data_select::data_select()
{
    state_table->setTable("test");
            state_table->setEditStrategy(QSqlTableModel::OnManualSubmit);
            state_table->select();
            state_table->setHeaderData(0,Qt::Horizontal,("num"));
            state_table->setHeaderData(1,Qt::Horizontal,("temp"));
            state_table->setHeaderData(2,Qt::Horizontal,("hum"));
            state_table->setHeaderData(3,Qt::Horizontal,("rain"));
            state_table->setHeaderData(4,Qt::Horizontal,("image"));

}

void data_select::temp_data(QTableView* table)
{

    table->setModel(state_table);
    table->hideColumn(0); table->hideColumn(2); table->hideColumn(3);table->hideColumn(4);
    table->show();
//    QSqlQuery qy( database::getInstance()->init_database());
//    qy.prepare("SELECT temp FROM test "
//               );
//    if(!qy.exec())
//    {

//        qDebug()<<qy.lastError();
//        qDebug()<<qy.lastQuery();
//        qDebug() << "temp select error";
//    }
//    else
//    {
//        qDebug () <<"temp_select!!!";}

}

void data_select::hum_data(QTableView *table)
{

    table->setModel(state_table);
    table->hideColumn(0); table->hideColumn(1); table->hideColumn(3);table->hideColumn(4);
    table->show();



}

void  data_select::rain_data(QTableView *table)
{
    table->setModel(state_table);
    table->hideColumn(0); table->hideColumn(1); table->hideColumn(2);table->hideColumn(4);
    table->show();



}

void  data_select::image_data(QTableView*table)
{
    table->setModel(state_table);
    table->hideColumn(0); table->hideColumn(1); table->hideColumn(2);table->hideColumn(3);
    table->show();


}

data_select::~data_select()
{
    delete state_table;
}

