#include "imagesqltablemodel.h"
#include <QPixmap>

ImageSqlTableModel::ImageSqlTableModel( QObject* parent, QSqlDatabase db )
    : QSqlTableModel( parent, db )
{
}

QVariant ImageSqlTableModel::data( const QModelIndex& index, int role ) const
{
    if ( index.column() == 4 && role == Qt::DecorationRole )
    {
        QPixmap outPixmap;
        outPixmap.loadFromData( this->index( index.row(), 4 ).data().toByteArray() );
        return outPixmap;
//        if ( role == Qt::SizeHintRole )
//        {
//            return outPixmap.size();
//        }
    }


    return QSqlTableModel::data( index, role );
}
