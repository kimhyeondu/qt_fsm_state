#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::on_pushButton_clicked);
    connect(ui->insert_button, &QPushButton::clicked, this, &MainWindow::on_insert_button_clicked);
    connect(ui->delet_button, &QPushButton::clicked, this, &MainWindow::on_delet_button_clicked);
    connect(ui->show_button, &QPushButton::clicked, this, &MainWindow::on_show_button_clicked);
    connect(ui->img_button, &QPushButton::clicked, this, &MainWindow::on_img_button_clicked);
    connect(ui->next_state, &QPushButton::clicked, this, &MainWindow::on_next_state_clicked);


//      connect( m_pStateMachine, &PcbStateMachine::signalTempMessage, this, &MainWindow::on_temp_data_print);
//       connect( m_pStateMachine, &PcbStateMachine::signalHumMessage, this, &MainWindow::on_hum_data_print);
//      connect( m_pStateMachine, &PcbStateMachine::signalRainMessage, this, &MainWindow::on_rain_data_print);
//      connect( m_pStateMachine, &PcbStateMachine::signalImageMessage, this, &MainWindow::on_img_data_print);
          connect( m_pStateMachine, &PcbStateMachine::signalTempMessage, this, &MainWindow::on_temp_data_print);
           connect( m_pStateMachine, &PcbStateMachine::signalHumMessage, this, &MainWindow::on_hum_data_print);
          connect( m_pStateMachine, &PcbStateMachine::signalRainMessage, this, &MainWindow::on_rain_data_print);
          connect( m_pStateMachine, &PcbStateMachine::signalImageMessage, this, &MainWindow::on_img_data_print);
}





MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_insert_button_clicked()
{

database::getInstance()->insert_database( ui->temp_edit->toPlainText(), ui->hum_edit->toPlainText(), ui->rain_edit->toPlainText());

}


void MainWindow::on_pushButton_clicked()
{

database::getInstance()->create_database(ui->textEdit->toPlainText());

}

void MainWindow::on_delet_button_clicked()
{
    database::getInstance()->delet_data();
}

void MainWindow ::on_show_button_clicked(){


    model->setTable("test");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    ui->tableWidget->setModel(model);

}

void MainWindow::on_img_button_clicked()
{
   database::getInstance()->load_img_File();

}


void MainWindow::on_temp_data_print(QPixmap temp_img)
{

    QStandardItem *item = new QStandardItem();
    item->setData(QVariant(temp_img), Qt::DecorationRole);
    Item_model->setItem(0, 0, item);
    ui->state_table->setModel(Item_model);


}

void MainWindow::on_hum_data_print(QPixmap hum_img)
{


//    model->setTable("test");
//    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    model->select();
//    ui->state_table->hideColumn(0);ui->state_table->hideColumn(1);ui->state_table->hideColumn(3);ui->state_table->hideColumn(4);
//    ui->state_table->setModel(model);

//    ui->state_table->setModel(data_box.state_table);
//    ui->state_table->hideColumn(0);ui->state_table->hideColumn(1);ui->state_table->hideColumn(3);ui->state_table->hideColumn(4);
//    ui->state_table->show();
    QStandardItem *item = new QStandardItem();
    item->setData(QVariant(hum_img), Qt::DecorationRole);
    Item_model->setItem(0, 1, item);
    ui->state_table->setModel(Item_model);

}

void MainWindow::on_rain_data_print(QPixmap rain_img)
{

//    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    model->select();
//    ui->state_table->hideColumn(0);ui->state_table->hideColumn(2);ui->state_table->hideColumn(1);ui->state_table->hideColumn(4);
//    ui->state_table->setModel(model);
//    ui->state_table->setModel(data_box.state_table);
//    ui->state_table->hideColumn(0);ui->state_table->hideColumn(2);ui->state_table->hideColumn(1);ui->state_table->hideColumn(4);
//    ui->state_table->show();
    QStandardItem *item = new QStandardItem();
    item->setData(QVariant(rain_img), Qt::DecorationRole);
    Item_model->setItem(0, 2, item);
    ui->state_table->setModel(Item_model);

}



void MainWindow::on_img_data_print(QPixmap img)
{


    QStandardItem *item = new QStandardItem();
    item->setData(QVariant(img), Qt::DecorationRole);
    Item_model->setItem(0, 3, item);
    ui->state_table->setModel(Item_model);
    ui->state_table->show();

}

void MainWindow::on_next_state_clicked()
{
m_pStateMachine->signalNextState();
}
