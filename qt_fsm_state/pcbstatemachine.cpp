
#include "pcbstatemachine.h"
#include <simutil.h>
#include <simvision2.h>
#include "database.h"
using namespace cv;
Q_LOGGING_CATEGORY( sm, "statemachine" )

PcbStateMachine::PcbStateMachine( QObject* parent ) : QObject( parent )
{
    initializeStateMachine();

    connect( m_pState00,      &QState::entered,              this, &PcbStateMachine::onS00StartEntered );
    connect( m_pState10,      &QState::entered,              this, &PcbStateMachine::onS10PrepareEntered );
    connect( m_pState20,      &QState::entered,              this, &PcbStateMachine::onS20LocationEntered );
    connect( m_pState30,      &QState::entered,              this, &PcbStateMachine::onS30DetectModeEntered );
    connect( m_pState31,      &QState::entered,              this, &PcbStateMachine::onS31ManualDetectEntered );
    connect( m_pTransRetry00, &QSignalTransition::triggered, this, &PcbStateMachine::signalNextState );
}

void PcbStateMachine::initializeStateMachine()
{
    // initial state
    m_pStateMachine->setInitialState( m_pStateRoot );
    m_pStateRoot->setInitialState( m_pState00 );
    // transition next
    m_pState00->addTransition( this, &PcbStateMachine::signalNextState,  m_pState10 );
    m_pState10->addTransition( this, &PcbStateMachine::signalNextState,  m_pState20 );
    m_pState20->addTransition( this, &PcbStateMachine::signalNextState,  m_pState30 );
    m_pState30->addTransition( this, &PcbStateMachine::signalNextState,  m_pState31 );
    m_pState31->addTransition( this, &PcbStateMachine::signalNextState,  m_pState00 );
    // transition retry
    m_pTransRetry00 = new QSignalTransition( this, &PcbStateMachine::signalRetryState, m_pState00 ); // m_pState00
    m_pStateRoot->addTransition( this, &PcbStateMachine::signalInitState, m_pState00 );
    //
    m_pStateMachine->start();
    qCInfo( sm ) << "[State Machine] Initialized and Started.";
}


void PcbStateMachine::onS00StartEntered()
{
    qCDebug( sm ) << QString( "[S00] StartEntered. ==================================================" );

}

void PcbStateMachine::onS10PrepareEntered()
{
        qCDebug( sm ) << "[S10] PrepareEntered";
     QString message ="temp_data_print";
      QTableView * table = new QTableView;
     QStandardItemModel *model = new QStandardItemModel;
     QPixmap pixmap_image;
     pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                    "/home",
                                                   "Images (*.png *.xpm *.jpg)"));

     QStandardItem *item = new QStandardItem();
     item->setData(QVariant(pixmap_image), Qt::DecorationRole);
     model->setItem(0, 0, item);
     table->setModel(model);
    // table->show();

    //table->show();
    emit signalTempMessage(pixmap_image);
    emit signalNextState();
}

void PcbStateMachine::onS20LocationEntered()
{
    QString message ="hum_data_print";
    QTableView * table = new QTableView;
   QStandardItemModel *model = new QStandardItemModel;
   QPixmap pixmap_image;
   pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                  "/home",
                                                 "Images (*.png *.xpm *.jpg)"));

   QStandardItem *item = new QStandardItem();
   item->setData(QVariant(pixmap_image), Qt::DecorationRole);
   model->setItem(0, 0, item);
   table->setModel(model);
  // table->show();
     emit signalHumMessage(pixmap_image);
    emit signalNextState();

}

void PcbStateMachine::onS30DetectModeEntered()
{
    QString message ="rain_data_print";
    QTableView * table = new QTableView;
   QStandardItemModel *model = new QStandardItemModel;
   QPixmap pixmap_image;
   pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                  "/home",
                                                 "Images (*.png *.xpm *.jpg)"));

   QStandardItem *item = new QStandardItem();
   item->setData(QVariant(pixmap_image), Qt::DecorationRole);
   model->setItem(0, 0, item);
   table->setModel(model);
   //table->show();
    //table->show();
    emit signalRainMessage(pixmap_image);
    emit signalNextState();
}

void PcbStateMachine::onS31ManualDetectEntered()
    {
    QString message ="img_data_print";
    QTableView * table = new QTableView;
   QStandardItemModel *model = new QStandardItemModel;
   QPixmap pixmap_image;
   pixmap_image.load(QFileDialog::getOpenFileName(nullptr, "Open File",
                                                  "/home",
                                                 "Images (*.png *.xpm *.jpg)"));

   QStandardItem *item = new QStandardItem();
   item->setData(QVariant(pixmap_image), Qt::DecorationRole);
   model->setItem(0, 0, item);
   table->setModel(model);
  // table->show();
    //table->show();
    emit signalImageMessage(pixmap_image);
    emit signalNextState();
    }

//void PcbStateMachine::onS31ManualDetectEntered()
//{
//    qCInfo( sm ) << "[S31] ManualDetectEntered";

//    emit signalNextState();

//}



