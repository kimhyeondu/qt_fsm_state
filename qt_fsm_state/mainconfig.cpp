#include "stable.h"
#include "mainconfig.h"

Q_LOGGING_CATEGORY( mainConfig, "config.main" )

MainConfig::MainConfig( const QString& fileName, QSettings::Format format, QObject* parent )
    : QObject( parent )
    , m_rMainSetting( fileName, format )
{
    m_rMainSetting.setIniCodec( "UTF-8" );
    QString defaultMainConfig = m_rMainSetting.fileName() + ".default";
    readAll();

    if ( QFile( m_rMainSetting.fileName() ).exists() )
    {
        if ( check.toLower() == "check" ) // normal
        {
            if ( !QFile::exists( defaultMainConfig ) ) { QFile::copy( m_rMainSetting.fileName(), defaultMainConfig ); }
        }
        else // abnormal
        {
            if ( QFile::exists( defaultMainConfig ) )
            {
                QFile::remove( m_rMainSetting.fileName() );
                QFile::copy( defaultMainConfig, m_rMainSetting.fileName() );
                readAll();
            }
            else
            {
                writeAll();

                if ( !QFile::exists( defaultMainConfig ) ) { QFile::copy( m_rMainSetting.fileName(), defaultMainConfig ); }
            }
        }

    }
    else // first execution only!!! write config file including default values.
    {
        writeAll();

        if ( !QFile::exists( defaultMainConfig ) ) { QFile::copy( m_rMainSetting.fileName(), defaultMainConfig ); }
    }
}

MainConfig::~MainConfig()
{
}

MainConfig& MainConfig::getInstance()
{
    static MainConfig ini;
    return ini;
}

void MainConfig::readAll()
{
    cameraSerailList.clear();

    if ( !m_rMainSetting.value( "Camera/Serial1", "" ).toString().trimmed().isEmpty() ) { cameraSerailList.append( m_rMainSetting.value( "Camera/Serial1" ).toString().trimmed() ); }
    if ( !m_rMainSetting.value( "Camera/Serial2", "" ).toString().trimmed().isEmpty() ) { cameraSerailList.append( m_rMainSetting.value( "Camera/Serial2" ).toString().trimmed() ); }
    if ( !m_rMainSetting.value( "Camera/Serial3", "" ).toString().trimmed().isEmpty() ) { cameraSerailList.append( m_rMainSetting.value( "Camera/Serial3" ).toString().trimmed() ); }

    // ----------------------------------------------------------------------
    defaultBarcodeCheck         = m_rMainSetting.value( "DefaultValue/BarcodeCheck",         1   ).toInt();
    defaultImageSave            = m_rMainSetting.value( "DefaultValue/ImageSave",            1   ).toInt();
    defaultSoundFlag            = m_rMainSetting.value( "DefaultValue/SoundFlag",            2   ).toInt();
    defaultImageRotate          = m_rMainSetting.value( "DefaultValue/ImageRotate",          0   ).toInt();
    defaultDetectionLine        = m_rMainSetting.value( "DefaultValue/DetectionLine",        48  ).toInt();
    defaultLocationType         = m_rMainSetting.value( "DefaultValue/LocationType",         0   ).toInt();
    defaultIsArrayForCompact    = m_rMainSetting.value( "DefaultValue/IsArrayForCompact",    0   ).toInt();
    defaultIsSimpleForCompact   = m_rMainSetting.value( "DefaultValue/IsSimpleForCompact",   0   ).toInt();
    defaultCompact2MaxLimit     = m_rMainSetting.value( "DefaultValue/Compact2MaxLimit",     70  ).toInt();
    defaultEmptyCheck           = m_rMainSetting.value( "DefaultValue/EmptyCheck",           1   ).toInt();
    defaultEmptyLimit           = m_rMainSetting.value( "DefaultValue/EmptyLimit",           70  ).toInt();
    defaultReverseCheck         = m_rMainSetting.value( "DefaultValue/ReverseCheck",         0   ).toInt();
    defaultReverseLimit         = m_rMainSetting.value( "DefaultValue/ReverseLimit",         0   ).toInt();
    defaultCapacitorCheck       = m_rMainSetting.value( "DefaultValue/CapacitorCheck",       0   ).toInt();
    defaultCapacitorLimit       = m_rMainSetting.value( "DefaultValue/CapacitorLimit",       70  ).toInt();
    defaultCapacitorMin         = m_rMainSetting.value( "DefaultValue/CapacitorMin",         100 ).toInt();
    defaultCapacitorMax         = m_rMainSetting.value( "DefaultValue/CapacitorMax",         200 ).toInt();
    defaultColorCheck           = m_rMainSetting.value( "DefaultValue/ColorCheck",           0   ).toInt();
    defaultColorLimit           = m_rMainSetting.value( "DefaultValue/ColorLimit",           75  ).toInt();
    defaultSearchRangeView      = m_rMainSetting.value( "DefaultValue/SearchRangeView",      0   ).toInt();
    defaultSearchRangeUpDown    = m_rMainSetting.value( "DefaultValue/SearchRangeUpDown",    25  ).toInt();
    defaultSearchRangeLeftRight = m_rMainSetting.value( "DefaultValue/SearchRangeLeftRight", 25  ).toInt();
    defaultRACThreshold         = m_rMainSetting.value( "DefaultValue/RACThreshold",         50  ).toInt();
    defaultRACMopologyRepeat    = m_rMainSetting.value( "DefaultValue/RACMopologyRepeat",    21  ).toInt();
    defaultBarcodeRow           = m_rMainSetting.value( "DefaultValue/BarcodeRow",           1   ).toInt();
    defaultBarcodeColumn        = m_rMainSetting.value( "DefaultValue/BarcodeColumn",        1   ).toInt();
    defaultBarcodeSize          = m_rMainSetting.value( "DefaultValue/BarcodeSize",          60  ).toInt();
    // ----------------------------------------------------------------------
    // operation
    TempImageSave               = m_rMainSetting.value( "Setting/TempImageSave",             0   ).toInt();
    DetectionMode               = m_rMainSetting.value( "Setting/DetectionMode",             0   ).toInt();
    ManualDetectDelay           = m_rMainSetting.value( "Setting/ManualDetectDelay",         0   ).toInt();
    RacBlackThreshold           = m_rMainSetting.value( "Setting/RACBlackThreshold",         12  ).toInt();
    MinimumDetectSize           = m_rMainSetting.value( "Setting/MinimumDetectSize",         400 ).toInt();
    MainImageRoate              = m_rMainSetting.value( "Setting/MainImageRoate",            0   ).toInt();
    FlowDirection               = m_rMainSetting.value( "Setting/FlowDirection",             0   ).toInt();
    CompactModeMin              = m_rMainSetting.value( "Setting/CompactModeMin",            400 ).toInt();
    Compact2Degree              = m_rMainSetting.value( "Setting/Compact2Degree",            3   ).toInt();
    ExtendLocation              = m_rMainSetting.value( "Setting/ExtendLocation",            0   ).toInt();
    AutoManualDetectInSetting   = m_rMainSetting.value( "Setting/AutoManualDetectInSetting", 3   ).toInt();
    CommPort                    = m_rMainSetting.value( "Setting/CommPort",                  ""  ).toString().trimmed();
    EquipId                     = m_rMainSetting.value( "Setting/EquipId",                   ""  ).toString().trimmed();
    LastModelName               = m_rMainSetting.value( "Setting/LastModelName",             ""  ).toString().trimmed();
    // result image save
    ResultSavePath              = m_rMainSetting.value( "Setting/ResultSavePath",            "./save" ).toString().trimmed();
    KeepMonths                  = m_rMainSetting.value( "Setting/KeepMonths",                3   ).toInt();
    // logs
    InitLogLevel                = m_rMainSetting.value( "Setting/InitLogLevel",              "info" ).toString().trimmed();
    LogSavePath                 = m_rMainSetting.value( "Setting/LogSavePath",               "logs" ).toString().trimmed();
    // password
    SettingPassword             = m_rMainSetting.value( "Setting/SettingPW",                 "" ).toString().trimmed();
    // fail color
    NgColorEmptyRed             = m_rMainSetting.value( "Setting/NgColorEmptyRed",           255 ).toInt();
    NgColorEmptyGreen           = m_rMainSetting.value( "Setting/NgColorEmptyGreen",         0   ).toInt();
    NgColorEmptyBlue            = m_rMainSetting.value( "Setting/NgColorEmptyBlue",          0   ).toInt();
    NgColorReverseRed           = m_rMainSetting.value( "Setting/NgColorReverseRed",         255 ).toInt();
    NgColorReverseGreen         = m_rMainSetting.value( "Setting/NgColorReverseGreen",       0   ).toInt();
    NgColorReverseBlue          = m_rMainSetting.value( "Setting/NgColorReverseBlue",        0   ).toInt();
    NgColorColorRed             = m_rMainSetting.value( "Setting/NgColorColorRed",           255 ).toInt();
    NgColorColorGreen           = m_rMainSetting.value( "Setting/NgColorColorGreen",         0   ).toInt();
    NgColorColorBlue            = m_rMainSetting.value( "Setting/NgColorColorBlue",          0   ).toInt();
    // sound
    OkSoundPath                 = m_rMainSetting.value( "Setting/OkSoundPath",               "sound/ok.wav"      ).toString().trimmed();
    NgSoundPath                 = m_rMainSetting.value( "Setting/NgSoundPath",               "sound/ng.wav"      ).toString().trimmed();
    BarcodeSoundPath            = m_rMainSetting.value( "Setting/BarcodeSoundPath",          "sound/barcode.wav" ).toString().trimmed();//
    BarcodeErrorSound           = m_rMainSetting.value( "Setting/BarcodeErrorSound",         0 ).toInt();
    // background noise remove
    BgNoiseRemove               = m_rMainSetting.value( "Setting/BgNoiseRemove",             0   ).toInt();
    BgNoiseMinS                 = m_rMainSetting.value( "Setting/BgNoiseMinS",               4   ).toInt();
    BgNoiseMaxS                 = m_rMainSetting.value( "Setting/BgNoiseMaxS",              112 ).toInt();
    BgNoiseMinV                 = m_rMainSetting.value( "Setting/BgNoiseMinV",               70  ).toInt();
    BgNoiseMaxV                 = m_rMainSetting.value( "Setting/BgNoiseMaxV",              140 ).toInt();
    // watching
    WatchingSavePath            = m_rMainSetting.value( "Setting/WatchingSavePath",          "" ).toString().trimmed();
    WatchingDbIp                = m_rMainSetting.value( "Setting/WatchingDbIp",              "" ).toString().trimmed(); // 192.168.100.10
    WatchingDbName              = m_rMainSetting.value( "Setting/WatchingDbName",            "" ).toString().trimmed();
    WatchingDbId                = m_rMainSetting.value( "Setting/WatchingDbId",              "" ).toString().trimmed(); // admin
    WatchingDbPw                = m_rMainSetting.value( "Setting/WatchingDbPw",              "" ).toString().trimmed(); // pws21
    // enable view
    enabledReverseAll           = m_rMainSetting.value( "Setting/EnabledReverseAll",         true ).toBool();
    enabledReverseUpDown        = m_rMainSetting.value( "Setting/EnabledReverseUpDown",      true ).toBool();
    enabledReverseLeftRight     = m_rMainSetting.value( "Setting/EnabledReverseLeftRight",   true ).toBool();
    enabledReverseDetail        = m_rMainSetting.value( "Setting/EnabledReverseDetail",      true ).toBool();
    check                       = m_rMainSetting.value( "Check/Check",                       "" ).toString().trimmed();
}

void MainConfig::writeAll()
{
    if ( cameraSerailList.size() > 0 ) { m_rMainSetting.setValue( "Camera/Serial1", cameraSerailList[0].trimmed() ); }
    if ( cameraSerailList.size() > 1 ) { m_rMainSetting.setValue( "Camera/Serial2", cameraSerailList[1].trimmed() ); }
    if ( cameraSerailList.size() > 2 ) { m_rMainSetting.setValue( "Camera/Serial3", cameraSerailList[2].trimmed() ); }

    // ----------------------------------------------------------------------
    m_rMainSetting.setValue( "DefaultValue/BarcodeCheck",         defaultBarcodeCheck );
    m_rMainSetting.setValue( "DefaultValue/ImageSave",            defaultImageSave );
    m_rMainSetting.setValue( "DefaultValue/SoundFlag",            defaultSoundFlag );
    m_rMainSetting.setValue( "DefaultValue/ImageRotate",          defaultImageRotate );
    m_rMainSetting.setValue( "DefaultValue/DetectionLine",        defaultDetectionLine );
    m_rMainSetting.setValue( "DefaultValue/LocationType",         defaultLocationType );
    m_rMainSetting.setValue( "DefaultValue/IsArrayForCompact",    defaultIsArrayForCompact );
    m_rMainSetting.setValue( "DefaultValue/IsSimpleForCompact",   defaultIsSimpleForCompact );
    m_rMainSetting.setValue( "DefaultValue/Compact2MaxLimit",     defaultCompact2MaxLimit );
    m_rMainSetting.setValue( "DefaultValue/EmptyCheck",           defaultEmptyCheck );
    m_rMainSetting.setValue( "DefaultValue/EmptyLimit",           defaultEmptyLimit );
    m_rMainSetting.setValue( "DefaultValue/ReverseCheck",         defaultReverseCheck );
    m_rMainSetting.setValue( "DefaultValue/ReverseLimit",         defaultReverseLimit );
    m_rMainSetting.setValue( "DefaultValue/CapacitorCheck",       defaultCapacitorCheck );
    m_rMainSetting.setValue( "DefaultValue/CapacitorLimit",       defaultCapacitorLimit );
    m_rMainSetting.setValue( "DefaultValue/CapacitorMin",         defaultCapacitorMin );
    m_rMainSetting.setValue( "DefaultValue/CapacitorMax",         defaultCapacitorMax );
    m_rMainSetting.setValue( "DefaultValue/ColorCheck",           defaultColorCheck );
    m_rMainSetting.setValue( "DefaultValue/ColorLimit",           defaultColorLimit );
    m_rMainSetting.setValue( "DefaultValue/SearchRangeView",      defaultSearchRangeView );
    m_rMainSetting.setValue( "DefaultValue/SearchRangeUpDown",    defaultSearchRangeUpDown );
    m_rMainSetting.setValue( "DefaultValue/SearchRangeLeftRight", defaultSearchRangeLeftRight );
    m_rMainSetting.setValue( "DefaultValue/RACThreshold",         defaultRACThreshold );
    m_rMainSetting.setValue( "DefaultValue/RACMopologyRepeat",    defaultRACMopologyRepeat );
    m_rMainSetting.setValue( "DefaultValue/BarcodeRow",           defaultBarcodeRow );
    m_rMainSetting.setValue( "DefaultValue/BarcodeColumn",        defaultBarcodeColumn );
    m_rMainSetting.setValue( "DefaultValue/BarcodeSize",          defaultBarcodeSize );
    // ----------------------------------------------------------------------
    // operation
    m_rMainSetting.setValue( "Setting/TempImageSave",             TempImageSave );
    m_rMainSetting.setValue( "Setting/DetectionMode",             DetectionMode );
    m_rMainSetting.setValue( "Setting/ManualDetectDelay",         ManualDetectDelay );
    m_rMainSetting.setValue( "Setting/RACBlackThreshold",         RacBlackThreshold );
    m_rMainSetting.setValue( "Setting/MinimumDetectSize",         MinimumDetectSize );
    m_rMainSetting.setValue( "Setting/MainImageRoate",            MainImageRoate );
    m_rMainSetting.setValue( "Setting/FlowDirection",             FlowDirection );
    m_rMainSetting.setValue( "Setting/CompactModeMin",            CompactModeMin );
    m_rMainSetting.setValue( "Setting/Compact2Degree",            Compact2Degree );
    m_rMainSetting.setValue( "Setting/ExtendLocation",            ExtendLocation );
    m_rMainSetting.setValue( "Setting/AutoManualDetectInSetting", AutoManualDetectInSetting );
    m_rMainSetting.setValue( "Setting/CommPort",                  CommPort           .trimmed() );
    m_rMainSetting.setValue( "Setting/EquipId",                   EquipId            .trimmed() );
    m_rMainSetting.setValue( "Setting/LastModelName",             LastModelName      .trimmed() );
    // result image save
    m_rMainSetting.setValue( "Setting/ResultSavePath",            ResultSavePath     .trimmed() );
    m_rMainSetting.setValue( "Setting/KeepMonths",                KeepMonths );
    // logs
    m_rMainSetting.setValue( "Setting/InitLogLevel",              InitLogLevel       .trimmed() );
    m_rMainSetting.setValue( "Setting/LogSavePath",               LogSavePath        .trimmed() );
    // setting password
    m_rMainSetting.setValue( "Setting/SettingPW",                 SettingPassword    .trimmed() );
    // fail color
    m_rMainSetting.setValue( "Setting/NgColorEmptyRed",           NgColorEmptyRed      );
    m_rMainSetting.setValue( "Setting/NgColorEmptyGreen",         NgColorEmptyGreen    );
    m_rMainSetting.setValue( "Setting/NgColorEmptyBlue",          NgColorEmptyBlue     );
    m_rMainSetting.setValue( "Setting/NgColorReverseRed",         NgColorReverseRed    );
    m_rMainSetting.setValue( "Setting/NgColorReverseGreen",       NgColorReverseGreen  );
    m_rMainSetting.setValue( "Setting/NgColorReverseBlue",        NgColorReverseBlue   );
    m_rMainSetting.setValue( "Setting/NgColorColorRed",           NgColorColorRed      );
    m_rMainSetting.setValue( "Setting/NgColorColorGreen",         NgColorColorGreen    );
    m_rMainSetting.setValue( "Setting/NgColorColorBlue",          NgColorColorBlue     );
    // sound
    m_rMainSetting.setValue( "Setting/OkSoundPath",               OkSoundPath        .trimmed() );
    m_rMainSetting.setValue( "Setting/NgSoundPath",               NgSoundPath        .trimmed() );
    m_rMainSetting.setValue( "Setting/BarcodeSoundPath",          BarcodeSoundPath   .trimmed() );
    m_rMainSetting.setValue( "Setting/BarcodeErrorSound",         BarcodeErrorSound   );
    // background noise remove
    m_rMainSetting.setValue( "Setting/BgNoiseRemove",             BgNoiseRemove  );
    m_rMainSetting.setValue( "Setting/BgNoiseMinS",               BgNoiseMinS    );
    m_rMainSetting.setValue( "Setting/BgNoiseMaxS",               BgNoiseMaxS    );
    m_rMainSetting.setValue( "Setting/BgNoiseMinV",               BgNoiseMinV    );
    m_rMainSetting.setValue( "Setting/BgNoiseMaxV",               BgNoiseMaxV    );
    // watching
    m_rMainSetting.setValue( "Setting/WatchingSavePath",          WatchingSavePath   .trimmed() );
    m_rMainSetting.setValue( "Setting/WatchingDbIp",              WatchingDbIp       .trimmed() );
    m_rMainSetting.setValue( "Setting/WatchingDbName",            WatchingDbName     .trimmed() );
    m_rMainSetting.setValue( "Setting/WatchingDbId",              WatchingDbId       .trimmed() );
    m_rMainSetting.setValue( "Setting/WatchingDbPw",              WatchingDbPw       .trimmed() );
    // enable view
    m_rMainSetting.setValue( "Setting/EnabledReverseAll",         enabledReverseAll );
    m_rMainSetting.setValue( "Setting/EnabledReverseUpDown",      enabledReverseUpDown );
    m_rMainSetting.setValue( "Setting/EnabledReverseLeftRight",   enabledReverseLeftRight );
    m_rMainSetting.setValue( "Setting/EnabledReverseDetail",      enabledReverseDetail );
    // check
    m_rMainSetting.setValue( "Check/Check",                       "check" );
    m_rMainSetting.sync();
}

void MainConfig::writeLastModelName( const QString& modelname )
{
    LastModelName = modelname.trimmed();
    m_rMainSetting.setValue( "Setting/LastModelName", LastModelName );
    m_rMainSetting.sync();
}

bool MainConfig::writeSettingPassword( const QString& password )
{
    QString passwordUpperTrimmed = password.trimmed().toUpper();

    if ( passwordUpperTrimmed.isEmpty() )
    {
        qCWarning( mainConfig ) << "[PASSWORD] New Password is empty and Not saved.";
        return false;
    }
    else
    {
        SettingPassword = QString( QCryptographicHash::hash( passwordUpperTrimmed.toUtf8(), QCryptographicHash::Md5 ).toHex() );
        m_rMainSetting.setValue( "Setting/SettingPW", SettingPassword );
        m_rMainSetting.sync();
        qCInfo( mainConfig ) << "[PASSWORD] New Password is saved.";
        return true;
    }
}

bool MainConfig::checkPassword( const QString& password )
{
    QString blah   = QString( QCryptographicHash::hash( password.toUpper().trimmed().toUtf8(),    QCryptographicHash::Md5 ).toHex() );
    QString master = QString( QCryptographicHash::hash( QString( "sea.side" ).toUpper().toUtf8(), QCryptographicHash::Md5 ).toHex() );

    if ( SettingPassword == blah || blah == master )
    {
        qCInfo( mainConfig ) << "[PASSWORD] Check passed.";
        return true;
    }
    else
    {
        qCInfo( mainConfig ) << "[PASSWORD] Password is mismatched.";
        QMessageBox::information( NULL, tr( "password not equal" ), tr( "passowrd not equal. please retry..." ) );
        return false;
    }
}
